import express, { Express, Router, ErrorRequestHandler } from 'express'
import compression from 'compression'
import morgan from 'morgan'
import winston, { transports } from 'winston'
import inflector from 'json-inflector'
import { readFileSync } from 'fs'
import { NotFound, UnprocessableEntity } from 'http-errors'

import CafeService from './cafe-service'

const console = new transports.Console()
winston.add(console)

const data = readFileSync('locations.csv')

export default class App {
  public server: Express
  private port: number
  private cafeService: CafeService

  constructor(port: string) {
    this.port = parseInt(port, 10)
    this.server = express()
    this.cafeService = new CafeService(data)

    this.attachMiddleware()
    this.attachRoutes()
    this.attachErrorHandler()
  }

  public start() {
    this.server.listen(this.port)
    winston.log('info', `Kinsa Cafe REST API listening on port ${this.port}`)
  }

  private attachMiddleware() {
    const { server } = this
    server
      .use(express.urlencoded({ extended: false }))
      .use(express.json())
      .use(compression())
      .use(
        inflector({
          response: 'underscore',
        }),
      )
      .use(morgan('dev'))
  }

  private attachRoutes() {
    this.server.get('/up', (_, res) => res.sendStatus(200))
    this.server.use('/cafes', this.cafeRouter())
  }

  private cafeRouter() {
    const { create, read, update, destroy, findNearest } = this.cafeService
    const cafeRouter = Router()

    cafeRouter
      .post('/', create)
      .get('/:id', read)
      .delete('/:id', destroy)
      .put('/:id', update)
      .post('/find_nearest', findNearest)

    return cafeRouter
  }

  private attachErrorHandler() {
    this.server.use((err: ErrorRequestHandler, req, res, next) => {
      if (err instanceof NotFound) {
        res.status(404).send(err)
      }
      if (err instanceof UnprocessableEntity) {
        res.status(422).send(err)
      }
    })
  }
}
