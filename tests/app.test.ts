import dotenv from 'dotenv'
dotenv.config()

import chai from 'chai'
import chaiHttp from 'chai-http'
import { describe, it } from 'mocha'

import App from '../src/app'

chai.use(chaiHttp)
const { expect, request } = chai

export const { server } = new App('3000')

describe('Server', () => {
  it('check health route', async () => {
    const { status, text } = await request(server).get('/up')

    expect(status).to.equal(200)
    expect(text).to.equal('OK')
  })
})
