import chai from 'chai'
import { describe, it } from 'mocha'
import { server } from './app.test'

const { expect, request } = chai

const addresses = [
  {
    address: '535 Mission St., San Francisco, CA',
    responseLocation: { lat: 37.788866, lng: -122.39821 },
  },
  {
    address: '252 Guerrero St, San Francisco, CA 94103, USA',
    responseLocation: { lat: 37.7671252, lng: -122.4245135 },
  },
]

const mockResponse = location => {
  return {
    status: 200,
    json: { results: [{ geometry: { location } }] },
  }
}

export const createMockClient = () => {
  return {
    geocode({ address }) {
      return {
        asPromise() {
          const { responseLocation } = addresses.find(({ address: a }) => a === address)!
          return Promise.resolve(mockResponse(responseLocation))
        },
      }
    },
  }
}

describe('POST /find_nearest', () => {
  it('can find the nearest coffee shop', async () => {
    const { body } = await request(server)
      .post(`/cafes/find_nearest`)
      .send({
        address: '535 Mission St., San Francisco, CA',
      })

    const {
      distance,
      coffee_shop: { name, id },
    } = body

    expect(name).to.exist.and.to.eq('Red Door Coffee')
    expect(id).to.exist.and.to.eq(16)
    expect(Math.round(distance)).to.exist.and.to.eq(185)
  })

  it('can find the nearest coffee shop', async () => {
    const { body } = await request(server)
      .post(`/cafes/find_nearest`)
      .send({
        address: '252 Guerrero St, San Francisco, CA 94103, USA',
      })

    const {
      distance,
      coffee_shop: { name, id },
    } = body

    expect(name).to.exist.and.to.eq('Four Barrel Coffee')
    expect(id).to.exist.and.to.eq(28)
    expect(Math.round(distance)).to.exist.and.to.eq(225)
  })
})
